# BaseApp

Install Dependencies

```
yarn install
```

> ### Serving the app


```
yarn run dev
```

> ### Build

```
yarn run build
```
> ### Libraries Used

```
Next.js
React.js
Bulma.css
font-awesome
```
